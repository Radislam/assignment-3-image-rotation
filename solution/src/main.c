#include "bmp.h"
#include "file_worker.h"
#include "image.h"
#include "input_parser.h"
#include "rotate.h"
#include <inttypes.h>
#include <stdio.h>

int main(int argc, char **argv) {
  struct input_values input_values = parse_input(argc, argv);
  if (!input_values.is_valid) {
    fprintf(stderr, "Input parameters are not valid!\nPlease provide them in "
                    "format <input_file> <output_file> <angle>\n");
    return 1;
  }
  FILE *input_file = open_file(input_values.input_file, "rb");
  FILE *output_file = open_file(input_values.output_file, "wb");
  if (input_file == NULL) {
    fprintf(stderr, "Input file cannot be opened\n");
    return 1;
  }
  if (output_file == NULL) {
    fprintf(stderr, "Output file cannot be opened\n");
    return 1;
  }

  struct image image = {0};
  enum read_status read_status = from_bmp(input_file, &image);
  if (read_status != READ_OK) {
    fprintf(stderr, "File was not read successfully");
    return 1;
  }
  if (!close_file(input_file)) {
    fprintf(stderr, "Input file cannot be closed!\n");
    return 1;
  }

  struct image output_image = rotate(image, input_values.angle);
  if (!output_image.data) {
    free(output_image.data);
    fprintf(stderr, "Error while creating rotated image!\n");
    return 1;
  }

  enum write_status write_status = to_bmp(output_file, &output_image);
  free(output_image.data);
  if (write_status != WRITE_OK) {
    fprintf(stderr, "File was not written successfully\n");
    return 1;
  }

  if (!close_file(output_file)) {
    fprintf(stderr, "Output file cannot be closed\n");
    return 1;
  }

  return 0;
}
