#include "file_worker.h"
#include "input_parser.h"
#include <stdio.h>

FILE *open_file(char *file_name, char *file_mode) {
  return fopen(file_name, file_mode);
}

bool close_file(FILE *file_name) { return fclose(file_name) == 0; }
