#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#pragma once

FILE *open_file(char *file_name, char *file_mode);
bool close_file(FILE *file_name);
